import {
  ImageListItem,
  ImageListItemBar,
  Card,
  CardHeader,
  CardMedia,
  CardContent,
  Typography,
} from '@mui/material';

import { shapedYelpBusinessObject } from '@module3-milestone/types';

export interface BusinessDetailProps {
  business: shapedYelpBusinessObject;
}

export function BusinessDetail({
  business: { displayAddress, id, image, isClosed, name, phone, rating },
}: BusinessDetailProps) {
  return (
<<<<<<< HEAD
    <div>
      <ImageListItem key={id}>
        <img
          src={`${image}?w=248&fit=crop&auto=format`}
          srcSet={`${image}?w=248&fit=crop&auto=format&dpr=2 2x`}
          alt={name}
          loading="lazy"
        />
        <ImageListItemBar
          title={name}
          subtitle={<span>phone: {phone}</span>}
          position="below"
        />
      </ImageListItem>
      <div>Store is Curently: {isClosed ? 'CLOSED' : 'Open'}</div>
      <div>Rating: {rating}</div>
      <div>Address: {displayAddress}</div>
    </div>
=======
    <Card sx={{ maxWidth: 425 }}>
      <CardMedia
        component="img"
        height="194"
        image={`${image}?w=248&fit=crop&auto=format`}
        alt={name}
      />
      <CardHeader title={name} subheader={phone} />
      <CardContent>
        <div>
          <p>
            Address: {displayAddress[0]} {displayAddress[1]}
          </p>
          <p>Store is Currently: {isClosed ? 'CLOSED' : 'Open'}</p>
          <p>Rating: {rating}</p>
        </div>
      </CardContent>
    </Card>
>>>>>>> new_branch_for_project
  );
}

export default BusinessDetail;
