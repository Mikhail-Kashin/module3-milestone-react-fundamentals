import { render } from '@testing-library/react';

import BusinessesList from './businesses-list';

describe('BusinessesList', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<BusinessesList />);
    expect(baseElement).toBeTruthy();
  });
});
