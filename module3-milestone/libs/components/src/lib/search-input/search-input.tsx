import { Box, TextField } from '@mui/material';

export const SearchInput = () => {
  return (
    <Box>
      <TextField
        id="standard-search"
        label="Search field"
        type="search"
        variant="standard"
      />
    </Box>
  );
};

export default SearchInput;
