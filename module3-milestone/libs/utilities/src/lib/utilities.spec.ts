import { shapeYelpArray, jacksonvilleBuisnessesArray } from './utilities';

describe('jacksonvilleBuisnessesArray', () => {
  it('should be an array of business objects', () => {
    const firstObject = {
      id: 'GHsoAez7pyAf8d6Uz7N1JA',
      alias: 'maple-street-biscuit-company-san-marco-jacksonville',
      name: 'Maple Street Biscuit Company - San Marco',
      image_url:
        'https://s3-media4.fl.yelpcdn.com/bphoto/Szb7WihIfj3Kx39mYfEwFg/o.jpg',
      is_closed: false,
      url: 'https://www.yelp.com/biz/maple-street-biscuit-company-san-marco-jacksonville?adjust_creative=f6vhwAod4mFlX_Xq0UUEAg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=f6vhwAod4mFlX_Xq0UUEAg',
      review_count: 1019,
      categories: [
        {
          alias: 'southern',
          title: 'Southern',
        },
        {
          alias: 'breakfast_brunch',
          title: 'Breakfast & Brunch',
        },
        {
          alias: 'comfortfood',
          title: 'Comfort Food',
        },
      ],
      rating: 4.5,
      coordinates: {
        latitude: 30.3038463782095,
        longitude: -81.6539008666204,
      },
      transactions: ['delivery'],
      price: '$',
      location: {
        address1: '2004 San Marco Blvd',
        address2: '',
        address3: '',
        city: 'Jacksonville',
        zip_code: '32204',
        country: 'US',
        state: 'FL',
        display_address: ['2004 San Marco Blvd', 'Jacksonville, FL 32204'],
      },
      phone: '+19043981004',
      display_phone: '(904) 398-1004',
      distance: 3951.7254377961917,
    };
    expect(jacksonvilleBuisnessesArray[0]).toEqual(firstObject);
  });
});

describe('shapeYelpArray', () => {
  it('should shape data from the jacksonvilleBuisnessesArray into the expected obejct format', () => {
    const actual = shapeYelpArray(jacksonvilleBuisnessesArray);

    const expected = {
      displayAddress: ['2004 San Marco Blvd', 'Jacksonville, FL 32204'],
      id: 'GHsoAez7pyAf8d6Uz7N1JA',
      image:
        'https://s3-media4.fl.yelpcdn.com/bphoto/Szb7WihIfj3Kx39mYfEwFg/o.jpg',
      isClosed: false,
      name: 'Maple Street Biscuit Company - San Marco',
      phone: '+19043981004',
      rating: 4.5,
    };

    expect(actual[0]).toEqual(expected);
  });
});
